﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float speed; //プレイヤーの動くスピード

    private Vector3 Player_pos; //プレイヤーのポジション

    int HP=3;

    public GameObject WaterBallPrefab;//水風船のプレハブ
    public float shotSpeed;//ボールのスピード

    public GameObject[] SpongePrefab;//スポンジのプレハブ
    

    public int PlayerID;

    float mutekiTime = 0;
    //bool mutekiflag=false;

    GameObject SpawnPoint;//ボールの生成位置

    Vector3 prevPos;    //直前の位置

    
    // Start is called before the first frame update
    void Start()
    {
        Player_pos = GetComponent<Transform>().position; //最初の時点でのプレイヤーのポジションを取得
        prevPos = Player_pos;
        SpawnPoint = transform.GetChild(0).gameObject;

    }

    // Update is called once per frame
    void Update()
    {
        //移動処理
        {
            float x=0, z=0;
            if(PlayerID==0)
            {
                //入力値を取得
                x= Input.GetAxis("Horizontal");  //横方向の入力値（-1.0～1.0）
                z= Input.GetAxis("Vertical");    // 縦方向の入力値（-1.0～1.0）
            }
            if (PlayerID == 1)
            {
                x = Input.GetAxis("Horizontal2");  //横方向の入力値（-1.0～1.0）
                z = Input.GetAxis("Vertical2");    // 縦方向の入力値（-1.0～1.0）
            }


                //移動
                this.GetComponent<CharacterController>().SimpleMove(new Vector3(x, 0, z) * Time.deltaTime * speed);
        }

        //向き
        {
            //1フレームでどのくらいうごいたか（移動量）
            Vector3 move = new Vector3(transform.position.x - prevPos.x, 0, transform.position.z - prevPos.z);


            //動いてたら　（動いてないなら向き変えなくていい）
            if (move.magnitude > 0) 
            {
                //現在の位置＋移動量＝次のフレームで行く予定の位置　を向くように回転させる
                transform.LookAt(transform.position + move);

                //現在の位置を更新
                prevPos = transform.position;
            }
        }

        if((PlayerID==0&& Input.GetKeyDown(KeyCode.Space))||(PlayerID==1&& Input.GetKeyDown(KeyCode.KeypadEnter)))
        {
            GameObject WaterBall = Instantiate(WaterBallPrefab);//ボールの生成
            WaterBall.GetComponent<Rigidbody>().AddForce(transform.forward*shotSpeed);//右に10の力で押す
            WaterBall.transform.position = SpawnPoint.transform.position;//ボールの位置をスポーンポイントの位置にする

            WaterBall.GetComponent<BallController>().PlayerID = PlayerID;
        }


        mutekiTime += Time.deltaTime;

        //if(mutekiflag==true)
        //{
        //    mutekiTime += Time.deltaTime;
        //    if(mutekiTime>=3)
        //    {
        //        mutekiTime = 0;
        //    }              
        //}

        //if (mutekiTime>=1)
        //{
        //    mutekiflag = false;
        //    GetComponent<CharacterController>().enabled = true;
        //    GetComponent<BoxCollider>().enabled = true;
        //}

        if (PlayerID == 0)
        {
            if (Input.GetKeyDown(KeyCode.Q))
            {
                HP++;
                GameObject Sponge = Instantiate(SpongePrefab[HP - 1]);//HP-1番目のプレハブを生成 ここから下を無敵時間のスクリプトに書く
                Sponge.transform.parent = this.transform;//
                Sponge.transform.localPosition = Vector3.zero; //ここまで

            }

        }

        if(PlayerID==1)
        {
            if(Input.GetKeyDown(KeyCode.Keypad7))
            {
                HP++;
                Debug.Log(HP);
                GameObject Sponge = Instantiate(SpongePrefab[HP - 1]);//HP-1番目のプレハブを生成 ここから下を無敵時間のスクリプトに書く
                Sponge.transform.parent = this.transform;//
                Sponge.transform.localPosition = Vector3.zero; //ここまで
            }
           

        }

        //ガード
        if(PlayerID==0)
        {   
            if (Input.GetKey(KeyCode.LeftControl))
            {
                GameObject obj = transform.Find("Kasa").gameObject;
                obj.SetActive(true);
            }
            if(Input.GetKeyUp(KeyCode.LeftControl))
            {
                GameObject obj = transform.Find("Kasa").gameObject;
                obj.SetActive(false);
            }
        }
        if (PlayerID == 1)
        {
            if (Input.GetKey(KeyCode.KeypadPlus))
            {
                GameObject obj = transform.Find("Kasa").gameObject;
                obj.SetActive(true);
            }
            if (Input.GetKeyUp(KeyCode.KeypadPlus))
            {
                GameObject obj = transform.Find("Kasa").gameObject;
                obj.SetActive(false);
            }
        }

    }
    void OnCollisionEnter(Collision other)
    {
        //無敵状態なので何もしない
        if(mutekiTime < 1)
        {
            return;
        }

        if (other.gameObject.tag == "WaterBall"&&other.gameObject.GetComponent<BallController>().PlayerID!=PlayerID)//ぶつかったもののタグがWaterBallなら
        {
            Destroy(transform.GetChild(1).gameObject);//子供1を削除
            HP--;//HPをマイナス
            GameObject Sponge = Instantiate(SpongePrefab[HP - 1]);//HP-1番目のプレハブを生成 ここから下を無敵時間のスクリプトに書く
            Sponge.transform.parent = this.transform;//
            Sponge.transform.localPosition = Vector3.zero; //ここまで
            Destroy(other.gameObject);

            //mutekiflag = true;

            //GetComponent<CharacterController>().enabled = false;
            //GetComponent<BoxCollider>().enabled = false;
            mutekiTime = 0;


        }

    }


}
